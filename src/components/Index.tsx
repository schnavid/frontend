import React, { createContext, Context, Component } from "react";
import CharacterRenderer from "./character-renderer/CharacterRenderer";
import Character, { defaultCharacter } from "../data/Character";
import DragAndDrop from "./drag-and-drop/DragAndDrop";
import Navbar from "./navbar/Navbar";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faDAndD } from "@fortawesome/free-brands-svg-icons"
import { faQuestionCircle } from "@fortawesome/free-solid-svg-icons";
import Modal from "./modal/Modal";

export type Update = (reducer: (character: Character) => Character) => void;

export const CharacterContext: Context<{ character: Character, update: Update }> = createContext({ character: defaultCharacter(), update: (x) => { } });

export default class Index extends Component<{}, { character: Character, showModal: boolean }> {
    constructor(props: {}) {
        super(props);
        this.state = { character: null, showModal: false };
    }

    handleDrop(files: FileList) {
        files[0].text().then(v => { this.setState({ character: JSON.parse(v) }); });
    }

    update(x: (character: Character) => Character) {
        this.setState({ character: x(this.state.character) });
    }

    defaultChar() {
        this.setState({ character: defaultCharacter() });
    }

    saveChar() {
        const { character } = this.state;
        const fileName = "char";
        const json = JSON.stringify(character);
        const blob = new Blob([json], { type: "application/json" });
        const href = URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = href;
        link.download = fileName + ".json";
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }

    render() {
        return (this.state.character != null) ?
            <CharacterContext.Provider value={{ character: this.state.character, update: this.update.bind(this) }}>
                <Modal show={this.state.showModal} onClose={() => this.setState({showModal: false})}>
                    Hier könnte ihre Hilfe stehen.
                </Modal>
                <CharacterRenderer />
                <Navbar>
                    <FontAwesomeIcon icon={faDAndD} id="dnd" size="10x" />
                    <Navbar.Group alignment="left">
                        <Navbar.Heading>{"D&D Charakter Tool"}</Navbar.Heading>
                    </Navbar.Group>
                    <Navbar.Group alignment="right">
                        <button id="save-button" onClick={this.saveChar.bind(this)}>Save</button>
                        <button id="open-help" onClick={e => this.setState({showModal: true})}>
                            <FontAwesomeIcon icon={faQuestionCircle} size="10x" />
                        </button>
                    </Navbar.Group>
                </Navbar>
            </CharacterContext.Provider> :
            <DragAndDrop handleDrop={this.handleDrop.bind(this)}>
                <input type="file" accept=".json" onChange={e => this.handleDrop.bind(this)((e.target as HTMLInputElement).files)} />
                <button onClick={this.defaultChar.bind(this)}>New Character</button>
            </DragAndDrop>;
    }
}