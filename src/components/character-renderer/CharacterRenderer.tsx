import "./character-renderer.less";

import React, { Component } from "react";
import MainPage from "./main-page/MainPage";

interface CharacterRendererProps { }

interface CharacterRendererState {
    page: number;
}

export default class CharacterRenderer extends Component<CharacterRendererProps, CharacterRendererState> {
    constructor(props: CharacterRendererProps) {
        super(props);
        this.state = { page: 1 };
    }

    previousPage(e: any) {
        this.setState({ page: this.state.page - 1 });
    }

    nextPage(e: any) {
        this.setState({ page: this.state.page + 1 });
    }

    render() {
        const pages = [
            <div />,
            <MainPage />,
            <div />
        ];

        return <div id="character">
            {/* <button className="pageButton left main-colors" disabled={this.state.page == 0} onClick={this.previousPage.bind(this)}> {"<"} </button> */}
            {pages[this.state.page]}
            {/* <button className="pageButton right main-colors" disabled={this.state.page == 2} onClick={this.nextPage.bind(this)}> {">"} </button> */}
        </div>;
    }
}