import "./main-page.less"

import React from "react";
import MainInfo from "./main-info/MainInfo";
import MiddleColumn from "./middle-column/MiddleColumn";
import LeftColumn from "./left-column/LeftColumn";
import RightColumn from "./right-column/RightColumn";
import Inventory from "./inventory/Inventory";

export default function MainPage() {
    return <div id="main-page">
        <MainInfo />
        <div id="columns">
            <LeftColumn />
            <MiddleColumn />
            <RightColumn />
        </div>
        <Inventory />
    </div>;
}