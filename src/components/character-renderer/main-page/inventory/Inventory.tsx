import React from "react";
import Item from "../../../../data/Item";
import { CharacterContext } from "../../../Index";
import Character from "../../../../data/Character";

import "./inventory.less";

export default function Inventory(props: {}) {
    let rest = (character: Character) => Array.from({ length: (60 - character.items.length) }).map((x) => ({ name: "", number: 0, weight: ""} as Item));

    return <CharacterContext.Consumer>
        {({ character, update }) => <div className="container main-colors">
            <div id="inventory">
                <h3 className="header">Inventar</h3>
                <div className="headers">
                    <div className="header first">
                        <h5 className="name">Gegenstand / Ausrüstung</h5>
                        <h5 className="amount">Anz.</h5>
                        <h5 className="weight">Gew.</h5>
                    </div>
                    <div className="header second">
                        <h5 className="name">Gegenstand / Ausrüstung</h5>
                        <h5 className="amount">Anz.</h5>
                        <h5 className="weight">Gew.</h5>
                    </div>
                    <div className="header third">
                        <h5 className="name">Gegenstand / Ausrüstung</h5>
                        <h5 className="amount">Anz.</h5>
                        <h5 className="weight">Gew.</h5>
                    </div>
                </div>
                <div className="items">
                    {Array.from({length: 60}).map((x, index) => <div key={index} className="item">
                        <input type="text" className="name" value={character.items[index] ? character.items[index].name : ""} onChange={e => update(character => {
                            if (character.items[index]) {
                                character.items[index].name = e.target.value;
                            } else {
                                character.items[index] = {name: e.target.value, number: 0, weight: ""};
                            }

                            return character;
                        })} />
                        <input type="number" className="amount" value={character.items[index] ? (character.items[index].number == 0 ? "" : character.items[index].number) : ""} onChange={e => update(character => {
                            if (character.items[index]) {
                                character.items[index].number = Number(e.target.value);
                            } else {
                                character.items[index] = {name: "", number: Number(e.target.value), weight: ""};
                            }

                            return character;
                        })} />
                        <input type="text" className="weight" value={character.items[index] ? character.items[index].weight : ""} onChange={e => update(character => {
                            if (character.items[index]) {
                                character.items[index].weight = e.target.value;
                            } else {
                                character.items[index] = {name: "", number: 0, weight: e.target.value};
                            }

                            return character;
                        })} />
                    </div>)}
                </div>
            </div>
        </div>}
    </CharacterContext.Consumer>;
}