import "./left-column.less";

import React from "react";
import Skills from "./skills/Skills";
import SavingThrows from "./saving-throws/SavingThrows";
import Numbers from "./numbers/Numbers";
import Proficiencies from "./proficiencies/Proficiencies";

export default function LeftColumn() {
    return <div id="left-column">
        <SavingThrows />
        <Numbers />
        <Skills />
        <Proficiencies />
    </div>
}