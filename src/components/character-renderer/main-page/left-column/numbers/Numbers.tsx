import "./numbers.less";

import React from "react";
import { CharacterContext } from "../../../../Index";

export default function Numbers() {
    return <CharacterContext.Consumer>
        {({ character, update }) => <div className="main-colors container">
            <div id="numbers">
                <div className="number">
                    <input type="number" className="value" value={character.armorClass} onChange={e => update((character) => {character.armorClass = Number(e.target.value); return character;})} />
                    <span className="label header">Rüstungsklasse</span>
                </div>
                <div className="number">
                    <input type="text" className="value" value={character.initiative} onChange={e => update((character) => {character.initiative = e.target.value; return character;})} />
                    <span className="label header">Initiative</span>
                </div>
                <div className="number">
                    <input type="number" className="value" value={character.speed} onChange={e => update((character) => {character.speed = Number(e.target.value); return character;})} />
                    <span className="label header">Bewegungsrate</span>
                </div>
            </div>
        </div>}
    </CharacterContext.Consumer>;
}