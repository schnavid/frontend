import "./proficiencies.less";

import React from "react";
import { CharacterContext } from "../../../../Index";

export default function Proficiencies() {
    return <CharacterContext.Consumer>
        {({ character, update }) => <div className="main-colors container">
            <h3 className="header">{"Übungen & Sprachen"}</h3>
            <div id="proficiencies">
            <h4 className="header">Rüstung</h4>
                <div className="line">
                    <div className="checkbox">
                        <input type="checkbox" name="lightArmor" checked={character.proficiencies.lightArmor} onChange={e => update((character) => { character.proficiencies.lightArmor = e.target.checked; return character; })}/>
                        <label htmlFor="lightArmor">Leichte</label>
                    </div>
                    <div className="checkbox">
                        <input type="checkbox" name="mediumArmor" checked={character.proficiencies.mediumArmor} onChange={e => update((character) => { character.proficiencies.mediumArmor = e.target.checked; return character; })}/>
                        <label htmlFor="mediumArmor">Mittlere</label>
                    </div>
                    <div className="checkbox">
                        <input type="checkbox" name="heavyArmor" checked={character.proficiencies.heavyArmor} onChange={e => update((character) => { character.proficiencies.heavyArmor = e.target.checked; return character; })}/>
                        <label htmlFor="heavyArmor">Schwere</label>
                    </div>
                    <div className="checkbox">
                        <input type="checkbox" name="shields" checked={character.proficiencies.shields} onChange={e => update((character) => { character.proficiencies.shields = e.target.checked; return character; })}/>
                        <label htmlFor="shields">Schilde</label>
                    </div>
                </div>
                <h4 className="header">Waffen</h4>
                <div className="line">
                    <div className="checkbox">
                        <input type="checkbox" name="simpleWeapons" checked={character.proficiencies.simpleWeapons} onChange={e => update((character) => { character.proficiencies.simpleWeapons = e.target.checked; return character; })}/>
                        <label htmlFor="simpleWeapons">Einfache</label>
                    </div>
                    <div className="checkbox">
                        <input type="checkbox" name="martialWeapons" checked={character.proficiencies.martialWeapons} onChange={e => update((character) => { character.proficiencies.martialWeapons = e.target.checked; return character; })}/>
                        <label htmlFor="martialWeapons">Kriegswaffen</label>
                    </div>
                    <span>Sonstige Waffen:</span>
                </div>
                <input type="text" className="otherWeapons" value={character.proficiencies.otherWeapons} onChange={e => update((character) => { character.proficiencies.otherWeapons = e.target.value; return character; })}/>
                <h4 className="header">{"Werkzeuge & Anderes"}</h4>
                <textarea rows={3} spellCheck={false} value={character.proficiencies.tools} onChange={e => update((character) => { character.proficiencies.tools = e.target.value; return character; })} />
                <h4 className="header">Sprachen</h4>
                <textarea rows={3} spellCheck={false} value={character.proficiencies.languages} onChange={e => update((character) => { character.proficiencies.languages = e.target.value; return character; })} />
            </div>
        </div>}
    </CharacterContext.Consumer>;
}