import React from "react";
import { CharacterContext, Update } from "../../../../Index";
import { Attribute, valueToMod, getAttribute, attributeName, attributeClass } from "../../../../../data/Attributes";
import Character, { getProficiencyBonus, modToString } from "../../../../../data/Character";

import "./saving-throws.less";

function savingThrowMod(attribute: Attribute, character: Character): string {
    let mod = valueToMod(getAttribute(attribute, character.attributes));
    if (character.savingThrows.indexOf(attribute) > -1) {
        mod += getProficiencyBonus(character);
    }
    return modToString(mod);
}

function SavingThrow(props: { attribute: Attribute, character: Character, update: Update }) {
    return <div className="saving-throw">
        <input type="checkbox" checked={props.character.savingThrows.indexOf(props.attribute) > -1} onChange={e => props.update((character) => {
            let index = character.savingThrows.indexOf(props.attribute);
            if (e.target.checked) {
                if (index == -1) {
                    character.savingThrows.push(props.attribute);
                }
            } else if (index > -1) {
                character.savingThrows.splice(index, 1);
            }

            return character;
        })} />
        <span className={`modifier ${attributeClass(props.attribute)}`}>{savingThrowMod(props.attribute, props.character)}</span>
        <span className="saving-throw-label">{attributeName(props.attribute)}</span>
    </div>
}

export default function SavingThrows() {
    return <CharacterContext.Consumer>
        {({ character, update }) => <div className="main-colors container">
            <h3 className="header">Rettungswürfe</h3>
            <div id="saving-throws">
                <SavingThrow attribute={Attribute.Strength} character={character} update={update}/>
                <SavingThrow attribute={Attribute.Dexterity} character={character} update={update}/>
                <SavingThrow attribute={Attribute.Constitution} character={character} update={update}/>
                <SavingThrow attribute={Attribute.Intelligence} character={character} update={update}/>
                <SavingThrow attribute={Attribute.Wisdom} character={character} update={update}/>
                <SavingThrow attribute={Attribute.Charisma} character={character} update={update}/>
            </div>
        </div>}
    </CharacterContext.Consumer>;
}