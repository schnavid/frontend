import React from "react";
import { CharacterContext, Update } from "../../../../Index";
import { Attribute, attributeShortName, getAttribute, valueToMod, attributeClass } from "../../../../../data/Attributes";
import Character, { getProficiencyBonus, modToString } from "../../../../../data/Character";

import "./skills.less";

const skillAttributes: { [key: string]: Attribute } = {
    acrobatics: Attribute.Dexterity,
    animalHandling: Attribute.Wisdom,
    arcana: Attribute.Intelligence,
    athletics: Attribute.Strength,
    deception: Attribute.Charisma,
    history: Attribute.Intelligence,
    insight: Attribute.Wisdom,
    intimidation: Attribute.Charisma,
    investigation: Attribute.Intelligence,
    medicine: Attribute.Wisdom,
    nature: Attribute.Intelligence,
    perception: Attribute.Wisdom,
    performance: Attribute.Charisma,
    persuasion: Attribute.Charisma,
    religion: Attribute.Intelligence,
    sleightOfHand: Attribute.Dexterity,
    stealth: Attribute.Dexterity,
    survival: Attribute.Wisdom,
}

const skillNames: { [key: string]: string } = {
    acrobatics: "Akrobatik",
    animalHandling: "Mit Tieren umgehen",
    arcana: "Arkane Kunde",
    athletics: "Athletik",
    deception: "Täuschen",
    history: "Geschichte",
    insight: "Motiv erkennen",
    intimidation: "Einschüchtern",
    investigation: "Nachforschungen",
    medicine: "Medizin",
    nature: "Natur",
    perception: "Wahrnehmung",
    performance: "Auftreten",
    persuasion: "Überzeugen",
    religion: "Religion",
    sleightOfHand: "Fingerfertigkeit",
    stealth: "Heimlichkeit",
    survival: "Überleben",
}

const skills: string[] = [
    "acrobatics",
    "animalHandling",
    "arcana",
    "athletics",
    "deception",
    "history",
    "insight",
    "intimidation",
    "investigation",
    "medicine",
    "nature",
    "perception",
    "performance",
    "persuasion",
    "religion",
    "sleightOfHand",
    "stealth",
    "survival",
]

function skillMod(skill: string, character: Character): string {
    let mod = valueToMod(getAttribute(skillAttributes[skill], character.attributes));
    if (character.skills.indexOf(skill) > -1) {
        mod += getProficiencyBonus(character);
    }
    return modToString(mod);
}

function Skill(props: { skill: string, character: Character, update: Update }) {
    return <div className="skill">
        <input type="checkbox" checked={props.character.skills.indexOf(props.skill) > -1} onChange={e => props.update((character) => {
            if (e.target.checked) {
                if (character.skills.indexOf(props.skill) == -1) {
                    character.skills.push(props.skill);
                }
            } else {
                let index = character.skills.indexOf(props.skill);
                if (index > -1) {
                    character.skills.splice(index, 1);
                }
            }
            return character;
        })} />
        <span className={`modifier ${attributeClass(skillAttributes[props.skill])}`}>{skillMod(props.skill, props.character)}</span>
        <span className="skill-label">{`${skillNames[props.skill]} (${attributeShortName(skillAttributes[props.skill])})`}</span>
    </div>
}

export default function Skills() {
    return <CharacterContext.Consumer>
        {({ character, update }) => <div id="skills" className="main-colors container">
            <h3 className="header">Fertigkeiten</h3>
            {skills.sort((a, b) => skillNames[a].localeCompare(skillNames[b])).map((x) => <Skill key={x} skill={x} character={character} update={update} />)}
        </div>}
    </CharacterContext.Consumer>
}