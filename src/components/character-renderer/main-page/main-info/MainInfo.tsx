import React from "react";
import { CharacterContext } from "../../../Index";
import Alignment from "../../../../data/Alignment";

import "./main-info.less";

export default function MainInfo() {
    return <CharacterContext.Consumer>
        {({ character, update }) => <div id="main-info" className="container main-colors">
            <div className="input-cell name">
                <input id="name-input" type="text" value={character.name} onChange={e => update((character) => {character.name = e.target.value; return character})} />
                <span className="input-label">Name</span>
            </div>
            <div className="input-cell">
                <input id="race-input" type="text" value={character.race} onChange={e => update((character) => {character.race = e.target.value; return character})} />
                <span className="input-label">Volk</span>
            </div>
            <div className="input-cell">
                <input id="background-input" type="text" value={character.background} onChange={e => update((character) => {character.background = e.target.value; return character})} />
                <span className="input-label">Hintergrund</span>
            </div>
            <div className="input-cell">
                <select value={character.alignment} onChange={e => update((character) => {character.alignment = Number.parseInt(e.target.value) as Alignment; return character})}>
                    <option value={Alignment.LawfulGood}>Rechtschaffen Gut</option>
                    <option value={Alignment.LawfulNeutral}>Rechtschaffen Neutral</option>
                    <option value={Alignment.LawfulEvil}>Rechtschaffen Böse</option>
                    <option value={Alignment.NeutralGood}>Neutral Gut</option>
                    <option value={Alignment.NeutralNeutral}>Neutral Neutral</option>
                    <option value={Alignment.NeutralEvil}>Neutral Böse</option>
                    <option value={Alignment.ChaoticGood}>Chaotisch Gut</option>
                    <option value={Alignment.ChaoticNeutral}>CHaotisch Neutral</option>
                    <option value={Alignment.ChaoticEvil}>Chaotisch Böse</option>
                </select>
                <span className="input-label">Gesinnung</span>
            </div>
            <div className="input-cell classes">
                <input id="classes-input" type="text" value={character.classes} onChange={e => update((character) => {character.classes = e.target.value; return character})} />
                <span className="input-label">Klasse(n)</span>
            </div>
            <div className="input-cell">
                <input id="player-input" type="text" value={character.playerName} onChange={e => update((character) => {character.playerName = e.target.value; return character})} />
                <span className="input-label">Spielername</span>
            </div>
        </div>}
    </CharacterContext.Consumer>;
}