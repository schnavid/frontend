import React from "react";
import Attributes from "./attributes/Attributes";
import Attacks from "./attacks/Attacks";
import Experience from "./experience/Experience";
import Hitpoints from "./hitpoints/Hitpoints";

import "./middle-column.less"

export default function MiddleColumn() {
    return <div id="middle-column">
        <Hitpoints />
        <Attributes />
        <Experience />
        <Attacks />
    </div>
}