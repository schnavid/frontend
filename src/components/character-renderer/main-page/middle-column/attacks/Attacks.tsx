import React from "react";
import { CharacterContext, Update } from "../../../../Index";
import AttackData from "../../../../../data/Attack";
import Character, { getProficiencyBonus, modToString } from "../../../../../data/Character";
import { getAttribute, attributeShortName, valueToMod, Attribute, attributeClass } from "../../../../../data/Attributes";

import "./attacks.less";
import { SortableElement, SortableContainer } from "react-sortable-hoc";
import arrayMove from "array-move";

function attackMod(attack: AttackData, character: Character): string {
    let mod = valueToMod(getAttribute(attack.attribute, character.attributes));
    if (attack.proficient) {
        mod += getProficiencyBonus(character);
    }
    return modToString(mod);
}

function autoGrow(element: HTMLElement) {
    element.style.height = "5px";
    element.style.height = (element.scrollHeight) + "px";
}

function Attack(props: { attack: AttackData, character: Character, update: Update, attackIndex: number }) {
    return <div className="attack">
        <div className="first-line">
            <div className="attack-header name">
                {props.attackIndex == 0 ? <h5>Name</h5> : null}
                <input type="text" value={props.attack.name} className="name" onChange={e => props.update((character) => { character.attacks[props.attackIndex].name = e.target.value; return character; })} />
            </div>
            <div className="attack-header">
                {props.attackIndex == 0 ? <h5>Üb.</h5> : null}
                <input type="checkbox" checked={props.attack.proficient} onChange={e => props.update((character) => { character.attacks[props.attackIndex].proficient = e.target.checked; return character; })} />
            </div>
            <div className="attack-header">
                {props.attackIndex == 0 ? <h5>Attr.</h5> : null}
                <select value={props.attack.attribute} onChange={e => props.update((character) => { character.attacks[props.attackIndex].attribute = Number(e.target.value) as Attribute; return character; })}>
                    <option value={Attribute.Strength}>{attributeShortName(Attribute.Strength)}</option>
                    <option value={Attribute.Dexterity}>{attributeShortName(Attribute.Dexterity)}</option>
                    <option value={Attribute.Constitution}>{attributeShortName(Attribute.Constitution)}</option>
                    <option value={Attribute.Intelligence}>{attributeShortName(Attribute.Intelligence)}</option>
                    <option value={Attribute.Wisdom}>{attributeShortName(Attribute.Wisdom)}</option>
                    <option value={Attribute.Charisma}>{attributeShortName(Attribute.Charisma)}</option>
                </select>
            </div>
            <div className="attack-header">
                {props.attackIndex == 0 ? <h5>Schaden</h5> : null}
                <input type="text" value={props.attack.damage} onChange={e => props.update((character) => { character.attacks[props.attackIndex].damage = e.target.value; return character; })} />
            </div>
            <div className="attack-header">
                {props.attackIndex == 0 ? <h5>Bonus</h5> : null}
                <span className={`modifier ${attributeClass(props.attack.attribute)}`}>{attackMod(props.attack, props.character)}</span>
            </div>
        </div>
        <div className="second-line">
            <div className="attack-header">
                {props.attackIndex == 0 ? <h5>Beschreibung</h5> : null}
                <textarea value={props.attack.description} spellCheck="false" onChange={e => props.update((character) => { autoGrow(e.target); character.attacks[props.attackIndex].description = e.target.value; return character; })} />
            </div>
            <button className="remove-attack" onClick={e => props.update((character) => { character.attacks.splice(props.attackIndex, 1); return character; })}>X</button>
        </div>
    </div>;
}

const SortableAttack = SortableElement(Attack);

const SortableAttacks = SortableContainer(({update, character}: {update: Update, character: Character}) => <div>
    {character.attacks.map((x, index) => <SortableAttack key={index} attack={x} attackIndex={index} index={index} update={update} character={character}/>)}
</div>);

export default function Attacks() {
    const onSortEnd = (update: Update, {oldIndex, newIndex}: {oldIndex: number, newIndex: number}) => {
        update((character: Character) => {
            character.attacks = arrayMove(character.attacks, oldIndex, newIndex);
            return character;
        })
    }

    return <CharacterContext.Consumer>
        {({ character, update }) => <div id="attacks" className="main-colors container">
            <h3 className="header">Angriffe</h3>
            <SortableAttacks character={character} update={update} onSortEnd={indices => onSortEnd(update, indices)} distance={5} shouldCancelStart={e => false} />
            <button className="add-attack" onClick={e => update((character) => { character.attacks.push({ name: "Schlag", description: "Harter Schlag", attribute: Attribute.Strength, proficient: false, damage: "1" }); return character; })}>+</button>
        </div>}
    </CharacterContext.Consumer>;
}