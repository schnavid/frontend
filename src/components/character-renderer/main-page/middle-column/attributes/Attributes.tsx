import "./attributes.less";

import React from "react";
import { Attribute, valueToModStr, getAttribute, attributeName, attributeClass } from "../../../../../data/Attributes";
import Character from "../../../../../data/Character";
import { Update, CharacterContext } from "../../../../Index";

function AttributeRenderer(props: { char: Character, attr: Attribute, update: Update }) {
    return <div className="attribute">
        <span className="attribute-name header">{attributeName(props.attr)}</span>
        <span className={`attribute-mod ${attributeClass(props.attr)}`}>{valueToModStr(getAttribute(props.attr, props.char.attributes))}</span>
        <input type="number" className="attribute-value" value={getAttribute(props.attr, props.char.attributes)} onChange={e => props.update((character) => {
            let value = (e.target as HTMLInputElement).value;
            switch (props.attr) {
                case Attribute.Strength: character.attributes.str = Number.parseInt(value); break;
                case Attribute.Dexterity: character.attributes.dex = Number.parseInt(value); break;
                case Attribute.Constitution: character.attributes.con = Number.parseInt(value); break;
                case Attribute.Intelligence: character.attributes.int = Number.parseInt(value); break;
                case Attribute.Wisdom: character.attributes.wis = Number.parseInt(value); break;
                case Attribute.Charisma: character.attributes.cha = Number.parseInt(value); break;
            } 
            return character;
        })} />
    </div>
}

export default function Attributes() {
    return <CharacterContext.Consumer>
        {({ character, update }) => <div className="main-colors container">
            <h3 className="header">Attribute</h3>
            <div id="attributes">
                <AttributeRenderer char={character} attr={Attribute.Strength} update={update} />
                <AttributeRenderer char={character} attr={Attribute.Dexterity} update={update} />
                <AttributeRenderer char={character} attr={Attribute.Constitution} update={update} />
                <AttributeRenderer char={character} attr={Attribute.Intelligence} update={update} />
                <AttributeRenderer char={character} attr={Attribute.Wisdom} update={update} />
                <AttributeRenderer char={character} attr={Attribute.Charisma} update={update} />
            </div>
        </div>}
    </CharacterContext.Consumer>;
}