import React from "react";

import "./experience.less";
import { CharacterContext } from "../../../../Index";
import { getLevel, getXpToNextLevel, getProficiencyBonus, modToString } from "../../../../../data/Character";

export default function Experience() {
    return <CharacterContext.Consumer>
        {({character, update}) => <div className="container main-colors">
            <h3 className="header">Erfahrung</h3>
            <div id="experience">
                <div className="input-cell">
                    <input type="text" id="current-experience" value={character.xp} onChange={e => update((character) => { character.xp = Number(e.target.value); return character; })}/>
                    <span className="header">Erfahrungspunkte</span>
                </div>

                <div className="second-line">
                    <div className="input-cell level">
                        <div id="level" className="main-colors">{getLevel(character.xp)}</div>
                        <span className="header">Level</span>
                    </div>
                    <div className="input-cell">
                        <div className="experience-field">{getXpToNextLevel(character.xp)}</div>
                        <span className="header">Nächstes Level</span>
                    </div>
                    <div className="input-cell">
                        <div className="experience-field">{modToString(getProficiencyBonus(character))}</div>
                        <span className="header">Übungsbonus</span>
                    </div>
                </div>
            </div>
        </div>}
    </CharacterContext.Consumer>
}