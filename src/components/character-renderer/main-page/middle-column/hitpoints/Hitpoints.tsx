import React from "react";

import "./hitpoints.less";
import { CharacterContext } from "../../../../Index";

export default function Hitpoints() {
    return <CharacterContext.Consumer>
        {({character, update}) => <div className="container main-colors">
            <h3 className="header">Trefferpunkte</h3>
            <div id="life">
                <div className="line">
                    <div className="input-cell current-hp">
                        <input type="number" value={character.currentHitpoints} onChange={e => update((character) => { character.currentHitpoints = Number(e.target.value); return character; })}/>
                        <span className="header">Aktuell</span>
                    </div>
                    <div className="input-cell temp-hp">
                        <input type="number" value={character.tempHitpoints} onChange={e => update((character) => { character.tempHitpoints = Number(e.target.value); return character; })}/>
                        <span className="header">Temp.</span>
                    </div>
                    <div className="input-cell max-hp">
                        <input type="number" value={character.maxHitpoints} onChange={e => update((character) => { character.maxHitpoints = Number(e.target.value); return character; })}/>
                        <span className="header">Maximum</span>
                    </div>
                </div>
                <h3 className="header">Trefferwürfel</h3>
                <div className="line">
                    <div className="input-cell">
                        <input type="text" value={character.currentHitdice} onChange={e => update((character) => { character.currentHitdice = e.target.value; return character; })}/>
                        <span className="header">Aktuell</span>
                    </div>
                    <div className="input-cell">
                        <input type="text" value={character.maxHitdice} onChange={e => update((character) => { character.maxHitdice = e.target.value; return character; })}/>
                        <span className="header">Maximum</span>
                    </div>
                </div>
            </div>
        </div>}
    </CharacterContext.Consumer>
}