import "./right-column.less";

import React from "react";
import Features from "./features/Features";

export default function RightColumn() {
    return <div id="right-column">
        <Features />
    </div>
}