import "./features.less";

import { CharacterContext, Update } from "../../../../Index";
import React, { Component } from "react";
import * as FeatureData from "../../../../../data/Feature";
const Remarkable = require("remarkable");
import { SortableContainer, SortableElement } from "react-sortable-hoc";
import arrayMove from "array-move";

import Character from "../../../../../data/Character";

function Markdown(props: { source: string }) {
    let markdown = new Remarkable.Remarkable().render(props.source);
    return <div dangerouslySetInnerHTML={{ __html: markdown }}></div>
}

class Feature extends Component<{ feature: FeatureData.default, update: Update, feature_index: number }, {isEditing: boolean}> {
    constructor(props: { feature: FeatureData.default, update: Update, feature_index: number }) {
        super(props);
        this.state = {
            isEditing: false,
        }
    }

    handleKeyDown(e: KeyboardEvent) {
        if (e.key == "Escape") {
            this.setState({isEditing: false});
        }
    }

    render() {
        return <section className="feature main-colors">
            {this.state.isEditing ? <div onKeyDown={e => this.handleKeyDown.bind(this)(e)}>
                <div className="buttons">
                    <button className="remove-feature" onClick={e => this.props.update((character) => { character.features.splice(this.props.feature_index, 1); this.setState({isEditing: false}); return character; })}>X</button>
                    <button className="finish-edit" onClick={e => this.setState({isEditing: false})}>Done</button>
                </div>
                <input type="text" name="name" placeholder="Name" value={this.props.feature.name} onChange={e => this.props.update(character => { character.features[this.props.feature_index].name = e.target.value; return character; })}/>
                <input type="text" name="source" placeholder="Quelle" value={this.props.feature.source} onChange={e => this.props.update(character => { character.features[this.props.feature_index].source = e.target.value; return character; })}/>
                <textarea rows={10} name="description" placeholder="Beschreibung" value={this.props.feature.description} onChange={e => this.props.update(character => { character.features[this.props.feature_index].description = e.target.value; return character; })} />
            </div> : <div onClick={() => this.setState({ isEditing: true })}>
                <span className="feature-header">{`${this.props.feature.name} (${this.props.feature.source})`}</span>
                <Markdown source={ this.props.feature.description || "**Click to edit!**"}/>
            </div>}
        </section>;
    }
}

const SortableFeature = SortableElement(Feature);

const SortableFeatures = SortableContainer(({features, update}: {features: FeatureData.default[], update: Update}) => <div>
    {features.map((x, index) => <SortableFeature index={index} key={index} feature={x} feature_index={index} update={update}/>)}
</div>);

export default function Features(props: {}) {
    const onSortEnd = (update: Update, {oldIndex, newIndex}: {oldIndex: number, newIndex: number}) => {
        update((character: Character) => {
            character.features = arrayMove(character.features, oldIndex, newIndex);
            return character;
        })
    }

    return <CharacterContext.Consumer>
        {({ character, update }) => <div id="features" className="main-colors container">
            <h3 className="header">Merkmale</h3>
            <SortableFeatures features={character.features} update={update} onSortEnd={indices => onSortEnd(update, indices)} distance={5} />
            <button className="add-feature" onClick={e => update((character) => { character.features.push({ name: "", description: "", source: "" }); return character; })}>+</button>
        </div>}
    </CharacterContext.Consumer>
}