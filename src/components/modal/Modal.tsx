import React, { PropsWithChildren } from "react";

import "./modal.less";

export default function Modal(props: PropsWithChildren<{ show: boolean, onClose: () => void }>) {
    return <div id="modal" className={props.show ? "container main-colors show" : "container main-colors"}>
        <section className="modal-main">
            <button onClick={props.onClose}>Close</button>
            {props.children}
        </section>
    </div>
}