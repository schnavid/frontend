import React, { Component } from "react";

import "./navbar.less";

class NavbarGroup extends Component<{alignment?: "left" | "center" | "right"}> {
    render() {
        return <div className={`navbar-group ${this.props.alignment}`}>
            {this.props.children}
        </div>;
    }
}

class NavbarHeading extends Component {
    render() {
        return <div className="navbar-heading">
            {this.props.children}
        </div>;
    }
}

export default class Navbar extends Component {
    public static Group = NavbarGroup;
    public static Heading = NavbarHeading;

    render() {
        return <nav className="navbar main-colors">
            {this.props.children}
        </nav>;
    }
}