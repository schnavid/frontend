import { Attribute } from "./Attributes";

export default interface Attack {
    name: string;
    attribute: Attribute;
    proficient: boolean;
    damage: string;
    description: string;
}