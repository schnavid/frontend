import { modToString } from "./Character";

export enum Attribute {
    Strength,
    Dexterity,
    Constitution,
    Intelligence,
    Wisdom,
    Charisma
}

export default interface Attributes {
    str: number;
    dex: number;
    con: number;
    int: number;
    wis: number;
    cha: number;
}

export function getAttribute(attr: Attribute, attrs: Attributes): number {
    switch (attr) {
        case Attribute.Strength: return attrs.str;
        case Attribute.Dexterity: return attrs.dex;
        case Attribute.Constitution: return attrs.con;
        case Attribute.Intelligence: return attrs.int;
        case Attribute.Wisdom: return attrs.wis;
        case Attribute.Charisma: return attrs.cha;
    }
}

export function valueToMod(val: number): number {
    return Math.floor((val - 10) / 2)
}

export function valueToModStr(val: number): string {
    let value = valueToMod(val);
    return modToString(value);
}

export function attributeName(attr: Attribute) {
    switch (attr) {
        case Attribute.Strength: return "Stärke";
        case Attribute.Dexterity: return "Geschicklichkeit";
        case Attribute.Constitution: return "Konstitution";
        case Attribute.Intelligence: return "Intelligenz";
        case Attribute.Wisdom: return "Weisheit";
        case Attribute.Charisma: return "Charisma";
    }
}

export function attributeClass(attr: Attribute) {
    switch (attr) {
        case Attribute.Strength: return "strength";
        case Attribute.Dexterity: return "dexterity";
        case Attribute.Constitution: return "constitution";
        case Attribute.Intelligence: return "intelligence";
        case Attribute.Wisdom: return "wisdom";
        case Attribute.Charisma: return "charisma";
    }
}

export function attributeShortName(val: Attribute): string {
    switch (val) {
        case Attribute.Strength: return "Str";
        case Attribute.Dexterity: return "Ges";
        case Attribute.Constitution: return "Kon";
        case Attribute.Intelligence: return "Int";
        case Attribute.Wisdom: return "Wei";
        case Attribute.Charisma: return "Cha";
    }
}