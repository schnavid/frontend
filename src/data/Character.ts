import Alignment from "./Alignment";
import Attributes, { Attribute } from "./Attributes";
import Feature from "./Feature";
import Attack from "./Attack";
import Item from "./Item";
import Proficiencies from "./Proficiencies";

export default interface Character {
    name: string;
    race: string;
    speed: number;
    inspiration: boolean;
    armorClass: number;
    initiative: string;
    maxHitpoints: number;
    tempHitpoints: number;
    currentHitpoints: number;
    maxHitdice: string;
    currentHitdice: string;
    xp: number;
    classes: string;
    background: string;
    playerName: string;
    alignment: Alignment;
    attributes: Attributes;
    skills: string[];
    features: Feature[];
    savingThrows: Attribute[];
    attacks: Attack[];
    items: Item[];
    proficiencies: Proficiencies;
}

export function defaultCharacter(): Character {
    return {
        name: "Kethi'il var Elryea",
        race: "Tiefling",
        speed: 9,
        inspiration: false,
        armorClass: 0,
        initiative: "+1",
        maxHitpoints: 1,
        tempHitpoints: 0,
        currentHitpoints: 1,
        maxHitdice: "1W8",
        currentHitdice: "1W8",
        xp: 0,
        classes: "Barde (1)",
        background: "Sonderling",
        playerName: "David",
        alignment: Alignment.ChaoticGood,
        attributes: {
            str: 7,
            dex: 13,
            con: 9,
            int: 12,
            wis: 10,
            cha: 20
        },
        skills: ["performance", "deception"],
        features: [
            {
                name: "Dunkelsicht (18m)",
                source: "PHB.43",
                description: "Dank deines infernalischen Erbes besitzt du eine überlegene Sicht in dunklen und dämmeigen Lichtverhältnissen. Behandle im Umkreis von 18 m dämmriges Licht wie helles Licht und Dunkelheit wie dämmriges Licht. Allerdings kannst du im Dunkeln keine Farben erkennen, nur Graustufen",
            },
            {
                name: "Höllische Resistenz",
                source: "PHB.43",
                description: "Du besitzt eine Resistenz gegen Feuerschaden",
            },
            {
                name: "Infernalisches Erbe",
                source: "PHB.43",
                description: "Du beherrschst den Zaubertrick *Thaumaturgie*. Ab der 3. Stufe kannst du dank dieses Merkmals den Spruch *Höllischer Tadel* des 2. Grades einmal wirken. Du musst eine lange beenden, bevor du dies erneut kannst. Mit Erreichen der 5. Stufe bist du in der Lage, den Zauber *Dunkelheit* auf die gleiche Weise einmal täglich einzusetzen. Das Attribut, mit dem du diese Zauber wirkst, ist Charisma.",
            },
        ],
        savingThrows: [Attribute.Charisma, Attribute.Dexterity],
        attacks: [
            {
                name: "Dolch",
                attribute: Attribute.Dexterity,
                damage: "1W4 Stich",
                proficient: true,
                description: "Finesse, leicht, Wurfwaffe (Reichweite 6/18)",
            },
            {
                name: "Rapier",
                attribute: Attribute.Dexterity,
                damage: "1W8 Stich",
                proficient: true,
                description: "Finesse",
            }
        ],
        items: [
            { name: "Deck of many things", number: 1, weight: "-" }
        ],
        proficiencies: {
            lightArmor: true,
            mediumArmor: false,
            heavyArmor: false,
            shields: false,
            simpleWeapons: true,
            martialWeapons: true,
            otherWeapons: "",
            languages: "Deutsch",
            tools: "",
        }
    };
}

export function getLevel(xp: number): number {
    if (xp < 300) return 1;
    else if (xp < 900) return 2;
    else if (xp < 2700) return 3;
    else if (xp < 6500) return 4;
    else if (xp < 14000) return 5;
    else if (xp < 23000) return 6;
    else if (xp < 34000) return 7;
    else if (xp < 48000) return 8;
    else if (xp < 64000) return 9;
    else if (xp < 85000) return 10;
    else if (xp < 100000) return 11;
    else if (xp < 120000) return 12;
    else if (xp < 140000) return 13;
    else if (xp < 165000) return 14;
    else if (xp < 195000) return 15;
    else if (xp < 225000) return 16;
    else if (xp < 265000) return 17;
    else if (xp < 305000) return 18;
    else if (xp < 355000) return 19;
    else return 20;
}

export function getXpToNextLevel(xp: number): number {
    if (xp < 300) return 300;
    else if (xp < 900) return 900;
    else if (xp < 2700) return 2700;
    else if (xp < 6500) return 6500;
    else if (xp < 14000) return 14000;
    else if (xp < 23000) return 23000;
    else if (xp < 34000) return 34000;
    else if (xp < 48000) return 48000;
    else if (xp < 64000) return 64000;
    else if (xp < 85000) return 85000;
    else if (xp < 100000) return 100000;
    else if (xp < 120000) return 120000;
    else if (xp < 140000) return 140000;
    else if (xp < 165000) return 165000;
    else if (xp < 195000) return 195000;
    else if (xp < 225000) return 225000;
    else if (xp < 265000) return 265000;
    else if (xp < 305000) return 305000;
    else if (xp < 355000) return 355000;
    else return 0;
}

export function getProficiencyBonus(character: Character): number {
    let level = getLevel(character.xp);
    if (level < 5) return 2;
    else if (level < 9) return 3;
    else if (level < 13) return 4;
    else if (level < 17) return 5;
    else return 6;
}

export function modToString(mod: number): string {
    if (mod < 0) return mod.toString();
    else return "+" + mod.toString();
}