export default interface Item {
    name: string,
    number: number,
    weight: string,
}