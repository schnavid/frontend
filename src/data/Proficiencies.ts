export default interface Proficiencies {
    lightArmor: boolean;
    mediumArmor: boolean;
    heavyArmor: boolean;
    shields: boolean;
    simpleWeapons: boolean;
    martialWeapons: boolean;
    otherWeapons: string;
    languages: string;
    tools: string;
}